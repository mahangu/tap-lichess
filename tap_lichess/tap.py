"""Lichess tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_lichess.streams import (
    LichessStream,
    GamesStream,
    UserStream
)

STREAM_TYPES = [
    GamesStream,
    UserStream
]


class TapLichess(Tap):
    """Lichess tap class."""
    name = "tap-lichess"

    config_jsonschema = th.PropertiesList(
        th.Property("start_date", th.DateTimeType, default="2021-07-01",
                    required=True),
        th.Property("lichess_username", th.StringType,
                    default="DrNykterstein", required=True),
        th.Property("api_url", th.StringType,
                    default="https://lichess.org/api/"),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
