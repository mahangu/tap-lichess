"""Stream class for tap-lichess."""

import requests
import base64
from copy import deepcopy
from pathlib import Path
from typing import Any, Dict, Optional, Iterable
import json
import time

from singer_sdk.streams import RESTStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class LichessStream(RESTStream):
    """LichessStream stream class."""

    @property
    def url_base(self) -> str:

        base_url = self.config.get("api_url")

        return base_url

    @property
    def http_headers(self) -> dict:

        http_headers = {}

        http_headers["Accept"] = "application/x-ndjson"

        if self.config.get("user_agent"):
            http_headers["User-Agent"] = self.config.get("user_agent")

        return http_headers

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}

        if self.config.get("start_date"):

            start_date = self.config.get("start_date")

            self.logger.info("Start Date: " + start_date)

            pattern = '%Y-%m-%d'

            # Lichess requires the start date in epoch time
            epoch_miliseconds = int(time.mktime(
                time.strptime(start_date, pattern)) * 1000)

            params["since"] = epoch_miliseconds

            self.logger.info("Start Date Epoch Time: "
                             + str(epoch_miliseconds))

        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:

        response_text = response.text

        # The Lichess API returns newline delimited JSON,
        # which means we have to split lines here to get
        # individual JSON objects.

        json_lines = response_text.splitlines()

        json_data = []

        for line in json_lines:

            yield json.loads(line)

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""

        return {
            "lichess_username": self.config.get("lichess_username"),
        }


class GamesStream(LichessStream):

    path = "games/user/{lichess_username}"
    name = "games"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "games.json"


class UserStream(LichessStream):

    path = "user/{lichess_username}"
    name = "users"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "users.json"
