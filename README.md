# tap-lichess

`tap-lichess` is a Singer tap / Meltano extractor for the [the Lichess API](https://lichess.org/api). It's built with the Meltano [SDK](https://gitlab.com/meltano/sdk) with inspiration from [the BambooHR Singer tap by Derek Visch](https://gitlab.com/autoidm/autoidm-tap-bamboohr).

It currently only implements the `/games/user/` and `/user/` endpoints, though it can be expanded to include others.

## Installation

1. Install Meltano

To get started, head over to the [Meltano Getting Started Guide](https://meltano.com/docs/getting-started.html#select-entities-and-attributes-to-extract) and follow the steps there to install it:

`pip3 install meltano`

2. Initialize a new project and activate a virtual environment using it:

`meltano init demo-project`

`source ../.venv/bin/activate`

## Configuration

3. Edit your project's [`meltano.yml`](https://meltano.com/docs/project.html) file to add `tap-lichess` as a custom extractor to your project:

```
plugins:
  extractors:
  - name: tap-lichess
    namespace: tap-lichess
    pip_url: git+https://gitlab.com/mahangu/tap-lichess.git
    config:
      lichess_username: DrNykterstein
      start_date: 2021-07-01
```

— Here, we're also configuring the extractor to pull in all of [Magnus Carlsen's](https://lichess.org/@/DrNykterstein) games since the specified `start_date`.

After that, tell Meltano to install this custom extractor in your project:

`meltano install extractor tap-lichess`

## Execution

After the extractor is installed, you can invoke it to test it:

```bash
meltano invoke tap-lichess --version
```

— or add it to an EL(T) pipeline that outputs JSON using [the target-jsonl loader](https://meltano.com/plugins/loaders/jsonl.html):

```bash
meltano add loader target-jsonl

mkdir -p output

meltano elt tap-lichess target-jsonl

cat output/games.jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to
develop your own taps and targets.
